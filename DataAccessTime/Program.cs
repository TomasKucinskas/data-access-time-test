﻿using System;
using System.Diagnostics;

namespace DataAccessTime
{
    class Program
    {
        static void SetMemory2DHorizontal(ref int[] texture, long width, long height, int value)
        {
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    texture[y * width + x] = value;
                }
            }
        }

        static void SetMemory2DVertical(ref int[] texture, long width, long height, int value)
        {
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    texture[y * width + x] = value;
                }
            }
        }

        static void Main(string[] args)
        {
            Stopwatch stopwatch = new Stopwatch();

            int iterationCount = 14;
            long testRunCount = 2 << (iterationCount * 2);

            long width = 2;
            long height = 0;
            int[] texture;

            for (int i = 0; i < iterationCount; i++)
            {
                try
                {
                    height = width;
                    texture = new int[width * height];
                    Console.WriteLine("New texture - \tWidth:" + width + " Height:" + height);

                    stopwatch.Start();
                    for (int j = 0; j < testRunCount; j++)
                    {
                        SetMemory2DVertical(ref texture, width, height, 1);
                    }
                    Console.WriteLine("Vertical fill time - \t" + (decimal)((float)stopwatch.ElapsedMilliseconds / testRunCount) + "ms");

                    stopwatch.Restart();
                    for (int j = 0; j < testRunCount; j++)
                    {
                        SetMemory2DHorizontal(ref texture, width, height, 2);
                    }
                    Console.WriteLine("Horizontal fill time - \t" + (decimal)((float)stopwatch.ElapsedMilliseconds / testRunCount) + "ms");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Could not run test with texture of size " + width + "x" + height + " successfully: ");
                    Console.WriteLine(ex);
                }
                finally
                {
                    width *= 2;
                    testRunCount = Math.Max(testRunCount >> 2, 1);

                    Console.WriteLine();
                }
            }

            Console.WriteLine("Testing was finished");
            Console.ReadLine();
        }
    }
}
